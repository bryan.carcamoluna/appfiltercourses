from bs4 import BeautifulSoup
from .connection import *
import requests
import re
import os
import sqlite3
import unicodedata

class Search():

    def __init__(self):

        self.title = ''
        self.id =0

    def getHtml(self, url):


        # connect page U
        page = requests.get(url).text
        soup = BeautifulSoup(page,"lxml")
        courses_data=""

        #search title
        div1 = soup.find("div",attrs={"class": "field field--name-field-recursos field--type-entity-reference-revisions field--label-hidden field__items"})

        div2 = div1.find("div",attrs={"class":"container"})
        self.title = div2.find("h3").text

        if(self.validateUrlDate()):
             # get data courses
             div3 = div1.find("div",attrs={"class":"tab-content"})
             courses_data = div3.find_all("tr")
             return courses_data
        else:
            return courses_data


    def getFilterDataCourses(self,data):

        self.func = Connect()
        sqliteConnection = self.func.connectDB()
        cursor = sqliteConnection.cursor()
        stmt2 = 'select IdPeriodo from Periodos where Descripcion = "{0}" '.format(self.title)
        cursor.execute(stmt2)
        records = cursor.fetchall()
        for row in records:
            self.id = row[0]


        stmt = """insert into Courses(Codigo,Asignatura,Bloque1,Bloque2,Bloque3,IdPeriodo) values (?,?,?,?,?,?);"""

        for tr in data:
            td = tr.find_all('td')
            count=0
            data_tuple=()

            for i in td:
                count+=1
                data_tuple+=(i.text,)

                if(count == 5):
                    data_tuple+=(self.id,)
                    cursor.execute(stmt, data_tuple)
                    sqliteConnection.commit()


        cursor.close()



    def validateUrlDate(self):

        # connect with sqlite
        self.func = Connect()
        sqliteConnection = self.func.connectDB()
        cursor = sqliteConnection.cursor()
        smt = 'select * from Periodos where Descripcion = "{0}"'.format(self.title)

        cursor.execute(smt)
        records = cursor.fetchall()

        print(len(records))

        if(len(records) == 0):
            smt2 = 'insert into Periodos(Descripcion) values ("{0}")'.format(self.title)
            cursor.execute(smt2)
            sqliteConnection.commit()
            cursor.close()
            return True

        else:
            return False

    def get_Title(self):
        titulo = self.title
        return titulo

    def get_modelP(self):

        self.func = Connect()
        sqliteConnection = self.func.connectDB()
        cursor = sqliteConnection.cursor()
        smt = 'select Descripcion from Periodos'

        cursor.execute(smt)
        records = cursor.fetchall()

        listP = []

        for row in records:
            listP.append([row[0]])

        return listP







































       
