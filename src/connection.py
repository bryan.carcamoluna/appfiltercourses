from gi.repository import Gtk
import  sqlite3
import os

class Connect():

    def __init__(self):
        self.title =""


    def connectDB(self):

         try:
             pathroute = str(os.path.dirname(os.path.abspath(__file__)))+'/data/CoursesDB'
             sqliteConnection = sqlite3.connect(pathroute)

             return sqliteConnection


         except sqlite3.Error as error:
            dialogo = Gtk.MessageDialog(None,0,message_type = Gtk.MessageType.INFO,buttons=Gtk.ButtonsType.OK,text = titulo)
            dialogo.set_position(Gtk.WindowPosition.CENTER)
            dialogo.format_secondary_text("Error al conectarse a sqlite: ", error)
            dialogo.run()
            dialogo.destroy()


        
