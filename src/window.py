# window.py
#
# Copyright 2022 Brayan Carcamo Luna
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written
# authorization.

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
import  re
from .search import *



@Gtk.Template(resource_path='/org/carcamo/luna/window.ui')
class FiltercoursesWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'FiltercoursesWindow'

    btnCargar = Gtk.Template.Child("btnCargar")
    imglogo = Gtk.Template.Child("logoImg")
    btnSaveUrl = Gtk.Template.Child("btnSaveUrl")
    textAddUrl = Gtk.Template.Child("urlAdd")
    urlTable = Gtk.Template.Child("urlTable")
    liststoreNone = Gtk.Template.Child("liststoreNone")
    textUrlCombox = Gtk.Template.Child("textUrlCombox")
    cursosTable = Gtk.Template.Child("cursosTable")


    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.loadWindow()


    @Gtk.Template.Callback()
    def cargarDatosClick(self,*args):
        print("temporal")



    @Gtk.Template.Callback()
    def on_btnSaveUrl_clicked(self,widget):
        text_validate = "https://www.uned.ac.cr/oferta-anual-de-asignaturas-cuatrimestrales"
        path_url = self.textAddUrl.get_text()
        self.textAddUrl.set_text("")


        if(re.search(text_validate,path_url)) is not None:
            self.func = Search()
            self.data = self.func.getHtml(path_url)

            if(len(self.data) != 0):
               self.func.getFilterDataCourses(self.data)
               self.loadData()



               '''
                  self.urlTable.set_model(model=store)


               store = Gtk.ListStore(str)
               store.append(["Selecionar un Periodo Academico: "])
               store.append([self.func.get_Title()])

               self.textUrlCombox.set_model(model=store)
               self.textUrlCombox.set_active(0)
               '''



            else:
               self.msg("Duplicado","Perido academico ya existe")


        else:
            self.msg("Url Invalidad", "Debe ingresar una Url validad que corresponda al periodo académico")


    def msg(self,titulo,texto):
        dialogo = Gtk.MessageDialog(None,0,message_type = Gtk.MessageType.INFO,buttons=Gtk.ButtonsType.OK,text = titulo)
        dialogo.set_position(Gtk.WindowPosition.CENTER)
        dialogo.format_secondary_text(texto)
        dialogo.run()
        dialogo.destroy()

    def clearList(self):

        self.liststoreNone.clear()
        for column in self.cursosTable.get_columns():
            self.cursosTable.remove_column(column)


    def loadWindow(self):
        self.imglogo.set_from_resource('/org/carcamo/luna/logoC1.png')
        self.func = Search()
        render = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn(title="Periodo Academico",cell_renderer=render,text = 0)
        self.urlTable.append_column(column)
        self.loadData()

    def loadData(self):
        self.clearList()
        store_A =  self.urlTable.get_model()
        store_A.clear()
        store_B = self.textUrlCombox.get_model()
        store_B.clear()

        store_B.append(["Seleccionar un Periodo Academico: "])
        self.textUrlCombox.set_active(0)

        for row in self.func.get_modelP():
            store_A.append(row)
            store_B.append(row)




    @Gtk.Template.Callback()
    def changedPeriodo(self,widget):

        tree_iter = self.textUrlCombox.get_active_iter()
        if tree_iter is not None:
             idItem = self.textUrlCombox.get_active()
             if(idItem!=0):
               model = self.textUrlCombox.get_model()
               print("ComboBox selected item: %s" % (model[tree_iter][0]))











    
